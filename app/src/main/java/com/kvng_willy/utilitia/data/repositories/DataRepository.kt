package com.kvng_willy.utilitia.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kvng_willy.utilitia.data.network.MyApi
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataRepository( private val api: MyApi) {
    fun getStatus(): LiveData<String> {
        val jsonResponse = MutableLiveData<String>()
        api.getStatus().enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                jsonResponse.value = "An error occurred"
            }
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful) {
                    jsonResponse.value = response.body()?.string()
                }else {
                    jsonResponse.value = "An error occurred"
                }
            }

        })
        return jsonResponse
    }
}
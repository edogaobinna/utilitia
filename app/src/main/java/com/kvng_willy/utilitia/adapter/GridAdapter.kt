package com.kvng_willy.utilitia.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.kvng_willy.utilitia.R
import com.kvng_willy.utilitia.data.network.StatusResponse
import com.kvng_willy.utilitia.utility.hide
import java.text.DateFormat

class GridAdapter(private val statusResponse: ArrayList<StatusResponse>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRecyclerViewItems: ArrayList<StatusResponse>? = statusResponse
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameText: TextView = view.findViewById(R.id.nameText)
        var caText: TextView = view.findViewById(R.id.category)

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            val menuItemHolder = holder as MyViewHolder
            val item: StatusResponse = mRecyclerViewItems!![position]
            menuItemHolder.caText.text = "${item.category}"
            menuItemHolder.nameText.text = "${item.name}"
        } catch (e: Exception) {
        }
    }
    override fun getItemCount(): Int {
        return mRecyclerViewItems!!.size
    }
}
package com.kvng_willy.utilitia.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kvng_willy.utilitia.R
import com.kvng_willy.utilitia.data.network.StatusResponse
import com.kvng_willy.utilitia.databinding.ActivityMainBinding
import com.kvng_willy.utilitia.utility.hide
import com.kvng_willy.utilitia.utility.show
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MainActivity : AppCompatActivity(),KodeinAware, AuthListener {
    override val kodein by kodein()

    private val factory: MainViewModelFactory by instance<MainViewModelFactory>()
    private lateinit var viewmodel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewmodel = ViewModelProvider(this,factory).get(MainViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.authListener = this

        viewmodel.fetchStatusTask()

    }

    override fun onTaskReturn(message: LiveData<String>?) {
        message?.observe(this, Observer {
            if(it.equals("an error occurred",ignoreCase = true)){
                errorHold.show()
                fetchBar.hide()
                fetchText.text = getString(R.string.networkError)
            }else{
                viewmodel.saveData(it)
                Intent(this,HomeActivity::class.java).also {
                    startActivity(it)
                    finish()
                }
            }
        })
    }

    override fun onTask2Return(message: ArrayList<StatusResponse>) {

    }
}
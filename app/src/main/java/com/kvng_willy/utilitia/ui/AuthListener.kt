package com.kvng_willy.utilitia.ui

import androidx.lifecycle.LiveData
import com.kvng_willy.utilitia.data.network.StatusResponse

interface AuthListener {
    fun onTaskReturn(message:LiveData<String>?)
    fun onTask2Return(message:ArrayList<StatusResponse>)
}
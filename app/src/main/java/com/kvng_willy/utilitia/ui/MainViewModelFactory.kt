package com.kvng_willy.utilitia.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kvng_willy.utilitia.data.preferences.PreferenceProvider
import com.kvng_willy.utilitia.data.repositories.DataRepository

class MainViewModelFactory (
    private val prefs: PreferenceProvider,
    private val repository:DataRepository
    ): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MainViewModel(prefs,repository) as T
        }
}
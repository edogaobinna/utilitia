package com.kvng_willy.utilitia.ui

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import com.kvng_willy.utilitia.R
import com.kvng_willy.utilitia.data.network.StatusResponse
import com.kvng_willy.utilitia.databinding.ActivityDetailBinding
import kotlinx.android.synthetic.main.activity_detail.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class DetailActivity : AppCompatActivity(),KodeinAware,AuthListener {
    override val kodein by kodein()

    private val factory: MainViewModelFactory by instance<MainViewModelFactory>()
    private lateinit var viewmodel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#DC2F33")))
        ;

        val binding: ActivityDetailBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_detail
        )
        viewmodel = ViewModelProvider(this, factory).get(MainViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.authListener = this
        displayInfo()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onTaskReturn(message: LiveData<String>?) {

    }

    override fun onTask2Return(message: ArrayList<StatusResponse>) {

    }

    @SuppressLint("SetTextI18n")
    fun displayInfo(){
        nameText.text = intent.getStringExtra("Name")
        url.text = "Url: ${intent.getStringExtra("Url")}"
        code.text = "ResponseCode: ${intent.getStringExtra("Code")}"
        time.text = "ResponseTime: ${intent.getStringExtra("Time")}"
        clasStr.text = intent.getStringExtra("Class")
    }
}
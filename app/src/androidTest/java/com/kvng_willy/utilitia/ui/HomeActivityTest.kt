package com.kvng_willy.utilitia.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.kvng_willy.utilitia.R
import org.junit.Rule
import org.junit.Test

class HomeActivityTest{
    @Rule
    @JvmField
    var mActivityRule: ActivityScenarioRule<HomeActivity?>? = ActivityScenarioRule(HomeActivity::class.java)

    //run a test to check if UI views are displaying to the user
    @Test
    @Throws(Exception::class)
    fun checkToolbarDisplay() {
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
    }
}